﻿using System;

namespace ContactManager.Domain.Common
{
    public class AuditableEntity : BaseEntity
    {
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
