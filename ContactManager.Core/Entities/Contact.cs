﻿using ContactManager.Domain.Common;
using System;
using System.Collections.Generic;

namespace ContactManager.Domain.Entities
{
    public class Contact : AuditableEntity
    {
        public Contact()
        {
            PhoneNumbers = new HashSet<PhoneNumber>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }

        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}
