﻿using ContactManager.Domain.Common;
using System.Collections.Generic;

namespace ContactManager.Domain.Entities
{
    public class Country : BaseEntity
    {
        public Country()
        {
            PhoneNumbers = new HashSet<PhoneNumber>();
        }

        public string Name { get; set; }
        public string ISOAlpha2 { get; set; }
        public string ISOAlpha3 { get; set; }
        public string CountryCode { get; set; }

        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}
