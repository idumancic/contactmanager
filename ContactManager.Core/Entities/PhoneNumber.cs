﻿using ContactManager.Domain.Common;

namespace ContactManager.Domain.Entities
{
    public class PhoneNumber : AuditableEntity
    {
        public string Value { get; set; }
        public int ContactId { get; set; }
        public int CountryId { get; set; }

        public Contact Contact { get; set; }
        public Country Country { get; set; }
    }
}
