﻿using ContactManager.Application.Common.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace ContactManager.Infrastructure
{
    public class NotificationService : INotificationService 
    {
        private readonly IHubContext<NotificationHub> _hubContext;

        public NotificationService(IHubContext<NotificationHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public Task NotifyAsync(string message)
        {
            return _hubContext.Clients.All.SendAsync("notify", message);
        }
    }
}
