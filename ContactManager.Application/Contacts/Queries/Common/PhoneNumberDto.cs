﻿namespace ContactManager.Application.Contacts.Queries.Common
{
    public class PhoneNumberDto
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string CountryCode { get; set; }
        public int CountryId { get; set; }
    }
}
