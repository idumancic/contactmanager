﻿using ContactManager.Application.Contacts.Queries.Common;
using System;
using System.Collections.Generic;

namespace ContactManager.Application.Contacts.Common
{
    public class ContactDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public IEnumerable<PhoneNumberDto> PhoneNumbers { get; set; }
    }
}
