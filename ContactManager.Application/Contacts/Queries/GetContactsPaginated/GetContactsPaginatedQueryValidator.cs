﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ContactManager.Application.Contacts.Queries.GetContactsPaginated
{
    public class GetContactsPaginatedQueryValidator : AbstractValidator<GetContactsPaginatedQuery>
    {
        public GetContactsPaginatedQueryValidator()
        {
            RuleFor(x => x.Page).NotEmpty();
            RuleFor(x => x.PageSize).NotEmpty();
        }
    }
}
