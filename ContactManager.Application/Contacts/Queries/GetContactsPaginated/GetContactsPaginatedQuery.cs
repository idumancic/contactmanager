﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ContactManager.Application.Common.Extensions;
using ContactManager.Application.Common.Interfaces;
using ContactManager.Application.Common.Models;
using ContactManager.Application.Contacts.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Contacts.Queries.GetContactsPaginated
{
    public class GetContactsPaginatedQuery : IRequest<PaginatedResult<ContactDto>>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }

        public class Handler : IRequestHandler<GetContactsPaginatedQuery, PaginatedResult<ContactDto>>
        {
            private readonly IContactManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IContactManagerContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<PaginatedResult<ContactDto>> Handle(GetContactsPaginatedQuery request, CancellationToken cancellationToken)
            {
                return await _context.Contacts
                    .AsNoTracking()
                    .ProjectTo<ContactDto>(_mapper.ConfigurationProvider)
                    .GetPaginatedAsync(request.Page, request.PageSize, cancellationToken);
            }
        }
    }
}
