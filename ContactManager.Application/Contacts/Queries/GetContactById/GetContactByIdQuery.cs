﻿using AutoMapper;
using ContactManager.Application.Common.Exceptions;
using ContactManager.Application.Common.Interfaces;
using ContactManager.Application.Contacts.Common;
using ContactManager.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Contacts.Queries.GetContactById
{
    public class GetContactByIdQuery : IRequest<ContactDto>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetContactByIdQuery, ContactDto>
        {
            private readonly IContactManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IContactManagerContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ContactDto> Handle(GetContactByIdQuery request, CancellationToken cancellationToken)
            {
                var contact = await _context.Contacts
                    .Include(x => x.PhoneNumbers)
                        .ThenInclude(x => x.Country)
                    .SingleOrDefaultAsync(x => x.Id == request.Id) ?? throw new NotFoundException(nameof(Contact), request.Id);

                return _mapper.Map<ContactDto>(contact);
            }
        }
    }
}
