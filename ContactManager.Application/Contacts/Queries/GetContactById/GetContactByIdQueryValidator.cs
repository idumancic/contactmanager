﻿using FluentValidation;

namespace ContactManager.Application.Contacts.Queries.GetContactById
{
    public class GetContactByIdQueryValidator : AbstractValidator<GetContactByIdQuery>
    {
        public GetContactByIdQueryValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}
