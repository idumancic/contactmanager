﻿using ContactManager.Application.Common.Exceptions;
using ContactManager.Application.Common.Interfaces;
using ContactManager.Application.Contacts.Commands.Common;
using ContactManager.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Contacts.Commands.UpdateContact
{
    public class UpdateContactCommand : IRequest
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public IEnumerable<PhoneNumberDto> PhoneNumbers { get; set; }

        public class Handler : IRequestHandler<UpdateContactCommand>
        {
            private readonly IContactManagerContext _context;

            public Handler(IContactManagerContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateContactCommand request, CancellationToken cancellationToken)
            {
                var contact = await _context.Contacts.FindAsync(request.Id) ?? throw new NotFoundException(nameof(Contact), request.Id);

                contact.FirstName = request.FirstName;
                contact.LastName = request.LastName;
                contact.DateOfBirth = request.DateOfBirth.Value;
                contact.Address = request.Address;
                contact.PhoneNumbers = request.PhoneNumbers
                        .Select(x => new PhoneNumber { Value = x.Value, CountryId = x.CountryId.Value })
                        .ToList();

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
