﻿using ContactManager.Application.Common.Interfaces;
using ContactManager.Application.Contacts.Commands.Common;
using FluentValidation;

namespace ContactManager.Application.Contacts.Commands.UpdateContact
{
    public class UpdateContactCommandValidator : AbstractValidator<UpdateContactCommand>
    {
        public UpdateContactCommandValidator(IContactManagerContext context)
        {
            RuleFor(x => x.FirstName)
                .MaximumLength(255)
                .NotEmpty();

            RuleFor(x => x.LastName)
                .MaximumLength(255)
                .NotEmpty();

            RuleFor(x => x.DateOfBirth)
                .NotNull();

            RuleFor(x => x.Address)
                .MaximumLength(255)
                .NotEmpty();

            RuleForEach(x => x.PhoneNumbers)
                .SetValidator(new PhoneNumberDtoValidator(context))
                .NotEmpty();
        }
    }
}
