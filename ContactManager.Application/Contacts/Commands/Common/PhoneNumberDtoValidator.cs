﻿using ContactManager.Application.Common.Interfaces;
using FluentValidation;

namespace ContactManager.Application.Contacts.Commands.Common
{
    public class PhoneNumberDtoValidator : AbstractValidator<PhoneNumberDto>
    {
        public PhoneNumberDtoValidator(IContactManagerContext context)
        {
            RuleFor(x => x.Value)
                .MaximumLength(100)
                .NotEmpty();

            RuleFor(x => x.CountryId)
                .MustAsync(async (countryId, cancellationToken) =>
                {
                    var country = await context.Countries.FindAsync(countryId);
                    return country != null;
                })
                .WithMessage(x => $"Country with id {x.CountryId} does not exist")
                .NotNull();
        }
    }
}
