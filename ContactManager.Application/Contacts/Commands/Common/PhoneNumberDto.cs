﻿namespace ContactManager.Application.Contacts.Commands.Common
{
    public class PhoneNumberDto
    {
        public string Value { get; set; }
        public int? CountryId { get; set; }
    }
}
