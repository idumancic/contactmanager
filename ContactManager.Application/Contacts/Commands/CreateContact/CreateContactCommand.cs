﻿using ContactManager.Application.Common.Interfaces;
using ContactManager.Application.Contacts.Commands.Common;
using ContactManager.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Contacts.Commands.CreateContact
{
    public class CreateContactCommand : IRequest<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public IEnumerable<PhoneNumberDto> PhoneNumbers { get; set; }

        public class Handler : IRequestHandler<CreateContactCommand, int>
        {
            private readonly IContactManagerContext _context;
            private readonly IMediator _mediator;

            public Handler(IContactManagerContext context, IMediator mediator)
            {
                _context = context;
                _mediator = mediator;
            }

            public async Task<int> Handle(CreateContactCommand request, CancellationToken cancellationToken)
            {
                var contact = new Contact
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    DateOfBirth = request.DateOfBirth.Value,
                    Address = request.Address,
                    PhoneNumbers = request.PhoneNumbers
                        .Select(x => new PhoneNumber { Value = x.Value, CountryId = x.CountryId.Value })
                        .ToList(),
                };

                _context.Contacts.Add(contact);
                await _context.SaveChangesAsync(cancellationToken);
                await _mediator.Publish(new ContactCreated { Id = contact.Id }, cancellationToken);
                return contact.Id;
            }
        }
    }
}
