﻿using ContactManager.Application.Common.Interfaces;
using ContactManager.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Contacts.Commands.CreateContact
{
    public class ContactCreated : INotification
    {
        public int Id { get; set; }

        public class ContactCreatedHandler : INotificationHandler<ContactCreated>
        {
            private readonly INotificationService _notification;

            public ContactCreatedHandler(INotificationService notification)
            {
                _notification = notification;
            }

            public async Task Handle(ContactCreated notification, CancellationToken cancellationToken)
            {
                await _notification.NotifyAsync($"{nameof(Contact)} has been created with id {notification.Id}");
            }
        }
    }
}
