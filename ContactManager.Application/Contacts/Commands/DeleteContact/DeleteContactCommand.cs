﻿using ContactManager.Application.Common.Exceptions;
using ContactManager.Application.Common.Interfaces;
using ContactManager.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Contacts.Commands.DeleteContact
{
    public class DeleteContactCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteContactCommand>
        {
            private readonly IContactManagerContext _context;

            public Handler(IContactManagerContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteContactCommand request, CancellationToken cancellationToken)
            {
                var contact = await _context.Contacts.FindAsync(request.Id) ?? throw new NotFoundException(nameof(Contact), request.Id);
                _context.Contacts.Remove(contact);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
