﻿using ContactManager.Domain.Common;
using System.Collections.Generic;

namespace ContactManager.Application.Common.Models
{
    public class PaginatedResult<T> where T : class
    {
        public PaginatedResult()
        {
            Items = new HashSet<T>();
        }

        public int TotalCount { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
