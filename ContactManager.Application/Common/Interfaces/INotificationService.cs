﻿using System.Threading.Tasks;

namespace ContactManager.Application.Common.Interfaces
{
    public interface INotificationService
    {
        Task NotifyAsync(string message);
    }
}
