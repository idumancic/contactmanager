﻿using ContactManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Common.Interfaces
{
    public interface IContactManagerContext
    {
        DbSet<Contact> Contacts { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<PhoneNumber> PhoneNumbers { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
