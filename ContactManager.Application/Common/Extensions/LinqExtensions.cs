﻿using ContactManager.Application.Common.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Application.Common.Extensions
{
    public static class LinqExtensions
    {
        public static async Task<PaginatedResult<T>> GetPaginatedAsync<T>(this IQueryable<T> query, int page, int pageSize, CancellationToken cancellationToken) where T : class
        {
            return new PaginatedResult<T>
            {
                TotalCount = query.Count(),
                Items = await query
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(cancellationToken)
            };
        }
    }
}
