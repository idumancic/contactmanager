﻿using AutoMapper;
using ContactManager.Application.Contacts.Common;
using ContactManager.Application.Contacts.Queries.Common;
using ContactManager.Domain.Entities;

namespace ContactManager.Application.Common.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Contact, ContactDto>();

            CreateMap<PhoneNumber, PhoneNumberDto>()
                .ForMember(dest => dest.CountryCode, opts => opts.MapFrom(x => x.Country.CountryCode));
        }
    }
}
