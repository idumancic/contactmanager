﻿using ContactManager.Application.Contacts.Commands.Common;
using ContactManager.Application.Contacts.Commands.CreateContact;
using ContactManager.Test.Common;
using MediatR;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace ContactManager.Test.Contacts.Commands
{
    public class CreateContactCommandTests : CommandTestBase
    {
        [Fact]
        public async Task ShouldRaiseContactCreatedNotification()
        {
            var mediatorMock = new Mock<IMediator>();
            var commandHandler = new CreateContactCommand.Handler(_context, mediatorMock.Object);

            var createdContactId = await commandHandler.Handle(new CreateContactCommand
            {
                FirstName = "Jane",
                LastName = "Doe",
                Address = "Beverly Hills 2a",
                DateOfBirth = DateTime.Now,
                PhoneNumbers = new List<PhoneNumberDto>()
                {
                    new PhoneNumberDto { Value = "2342342342", CountryId = 3 }
                }
            }, CancellationToken.None);


            mediatorMock.Verify(m => m.Publish(It.Is<ContactCreated>(x => x.Id == createdContactId), It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
