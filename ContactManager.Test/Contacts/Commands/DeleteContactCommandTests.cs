﻿using ContactManager.Application.Common.Exceptions;
using ContactManager.Application.Contacts.Commands.DeleteContact;
using ContactManager.Test.Common;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace ContactManager.Test.Contacts.Commands
{
    public class DeleteContactCommandTests : CommandTestBase
    {
        private readonly DeleteContactCommand.Handler _commandHandler;

        public DeleteContactCommandTests() : base()
        {
            _commandHandler = new DeleteContactCommand.Handler(_context);
        }

        [Fact]
        public async Task ShouldThrowNotFoundExceptionOnInvalidId()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _commandHandler.Handle(new DeleteContactCommand { Id = 999 }, CancellationToken.None));
        }

        [Fact]
        public async Task ShouldDeleteContact()
        {
            int validId = 1;
            
            var command = new DeleteContactCommand { Id = validId };
            await _commandHandler.Handle(command, CancellationToken.None);

            var customer = await _context.Contacts.FindAsync(validId);
            Assert.Null(customer);
        }
    }
}
