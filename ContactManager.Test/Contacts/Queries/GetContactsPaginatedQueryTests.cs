﻿using AutoMapper;
using ContactManager.Application.Common.Models;
using ContactManager.Application.Contacts.Common;
using ContactManager.Application.Contacts.Queries.GetContactsPaginated;
using ContactManager.Persistence;
using ContactManager.Test.Common;
using FluentAssertions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace ContactManager.Test.Contacts.Queries
{
    [Collection("QueryCollection")]
    public class GetContactsPaginatedQueryTests
    {
        private readonly ContactManagerContext _context;
        private readonly IMapper _mapper;

        public GetContactsPaginatedQueryTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _mapper = fixture.Mapper;
        }

        [Fact]
        public async Task ShouldGetContactsPaginated()
        {
            var commandHandler = new GetContactsPaginatedQuery.Handler(_context, _mapper);

            var result = await commandHandler.Handle(new GetContactsPaginatedQuery { Page = 1, PageSize = 10 }, CancellationToken.None);

            result.Should().BeOfType(typeof(PaginatedResult<ContactDto>));
            result.Items.Should().NotBeEmpty();
        }
    }
}
