﻿using AutoMapper;
using ContactManager.Application.Contacts.Common;
using ContactManager.Application.Contacts.Queries.GetContactById;
using ContactManager.Persistence;
using ContactManager.Test.Common;
using FluentAssertions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace ContactManager.Test.Contacts.Queries
{
    [Collection("QueryCollection")]
    public class GetContactByIdQueryTests
    {
        private readonly ContactManagerContext _context;
        private readonly IMapper _mapper;

        public GetContactByIdQueryTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _mapper = fixture.Mapper;
        }

        [Fact]
        public async Task ShouldGetContactById()
        {
            var commandHandler = new GetContactByIdQuery.Handler(_context, _mapper);

            var result = await commandHandler.Handle(new GetContactByIdQuery { Id = 1 }, CancellationToken.None);

            result.Should().BeOfType(typeof(ContactDto));
            result.Id.Should().Be(1);
        }
    }
}
