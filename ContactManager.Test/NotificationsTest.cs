using ContactManager.Infrastructure;
using Microsoft.AspNetCore.SignalR;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace ContactManager.Test
{
    public class NotificationsTest
    {
        [Fact]
        public async Task ShouldSendNotification()
        {
            var mockClients = new Mock<IHubClients>();
            var mockClientProxy = new Mock<IClientProxy>();
            mockClients.Setup(clients => clients.All).Returns(mockClientProxy.Object);

            var hubContext = new Mock<IHubContext<NotificationHub>>();
            hubContext.Setup(x => x.Clients).Returns(() => mockClients.Object);

            var notificationService = new NotificationService(hubContext.Object);
            await notificationService.NotifyAsync("This is the unit test");
            mockClients.Verify(clients => clients.All, Times.Once);
        }
    }
}
