using System;
using AutoMapper;
using ContactManager.Application.Common.Mappings;
using ContactManager.Persistence;
using Xunit;

namespace ContactManager.Test.Common
{
    public class QueryTestFixture : IDisposable
    {
        public ContactManagerContext Context { get; private set; }
        public IMapper Mapper { get; private set; }

        public QueryTestFixture()
        {
            Context = ContactManagerContextFactory.Create();

            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            Mapper = configurationProvider.CreateMapper();
        }

        public void Dispose()
        {
            ContactManagerContextFactory.Destroy(Context);
        }
    }

    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }
}