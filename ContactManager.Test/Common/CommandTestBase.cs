using ContactManager.Persistence;
using System;;

namespace ContactManager.Test.Common
{
    public class CommandTestBase : IDisposable
    {
        protected readonly ContactManagerContext _context;

        public CommandTestBase()
        {
            _context = ContactManagerContextFactory.Create();
        }

        public void Dispose()
        {
            ContactManagerContextFactory.Destroy(_context);
        }
    }
}