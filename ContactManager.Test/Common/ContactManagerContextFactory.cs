using System;
using ContactManager.Domain.Entities;
using ContactManager.Persistence;
using Microsoft.EntityFrameworkCore;

namespace ContactManager.Test.Common
{
    public class ContactManagerContextFactory
    {
        public static ContactManagerContext Create()
        {
            var options = new DbContextOptionsBuilder<ContactManagerContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new ContactManagerContext(options);

            context.Database.EnsureCreated();

            context.Countries.AddRange(new[] {
                new Country { Name = "Croatia", ISOAlpha2 = "HR", ISOAlpha3 = "HRV", CountryCode = "385" },
                new Country { Name = "United Kingdom", ISOAlpha2 = "GB", ISOAlpha3 = "GBR", CountryCode = "44" },
                new Country { Name = "United States", ISOAlpha2 = "US", ISOAlpha3 = "USA", CountryCode = "1" }
            });

            context.Contacts.AddRange(new[] {
                new Contact { FirstName = "John", LastName = "Doe", DateOfBirth = new System.DateTime(1971, 12, 1), Address = "Garrison Street 2", Created = DateTime.Now },
                new Contact { FirstName = "Charmaine", LastName = "Sandels", DateOfBirth = new System.DateTime(1996, 5, 17), Address = "Karstens Drive 68", Created = DateTime.Now },
                new Contact { FirstName = "Alexandre", LastName = "Desseine", DateOfBirth = new System.DateTime(1985, 10, 20), Address = "Arrowood Lane 45", Created = DateTime.Now }
            });

            context.PhoneNumbers.AddRange(new[]
            {
                new PhoneNumber { Value = "9537899612", CountryId = 3, ContactId = 1, Created = DateTime.Now },
                new PhoneNumber { Value = "9537899613", CountryId = 3, ContactId = 1, Created = DateTime.Now },
                new PhoneNumber { Value = "2744201872", CountryId = 2, ContactId = 2, Created = DateTime.Now },
                new PhoneNumber { Value = "0982342341", CountryId = 1, ContactId = 3, Created = DateTime.Now },
                new PhoneNumber { Value = "0982342342", CountryId = 1, ContactId = 3, Created = DateTime.Now },
                new PhoneNumber { Value = "2744201873", CountryId = 2, ContactId = 3, Created = DateTime.Now }
            });

            context.SaveChanges();

            return context;
        }

        public static void Destroy(ContactManagerContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}