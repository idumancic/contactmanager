﻿using ContactManager.Application.Contacts.Commands.CreateContact;
using ContactManager.Application.Contacts.Commands.DeleteContact;
using ContactManager.Application.Contacts.Commands.UpdateContact;
using ContactManager.Application.Contacts.Queries.GetContactById;
using ContactManager.Application.Contacts.Queries.GetContactsPaginated;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace ContactManager.Api.Controllers
{
    public class ContactController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPaginated(int page, int pageSize)
        {
            return Ok(await Mediator.Send(new GetContactsPaginatedQuery { Page = page, PageSize = pageSize }));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetContactByIdQuery { Id = id }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create([FromBody] CreateContactCommand command)
        {
            return CreatedAtAction(nameof(Create), new { Id = await Mediator.Send(command) });
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update([FromBody] UpdateContactCommand command)
        {
            await Mediator.Send(command);
            return Ok();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteContactCommand { Id = id });
            return NoContent();
        }
    }
}
