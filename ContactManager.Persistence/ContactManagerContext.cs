﻿using ContactManager.Application.Common.Interfaces;
using ContactManager.Domain.Common;
using ContactManager.Domain.Entities;
using ContactManager.Persistence.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ContactManager.Persistence
{
    public class ContactManagerContext : DbContext, IContactManagerContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }

        public ContactManagerContext(DbContextOptions<ContactManagerContext> options) : base(options) {}

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.Modified = DateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(ContactManagerContext).Assembly);
            builder.Seed();
        }
    }
}
