﻿using ContactManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ContactManager.Persistence.Configurations
{
    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.Property(x => x.FirstName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(x => x.LastName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(x => x.DateOfBirth)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(x => x.Address)
                .HasMaxLength(255)
                .IsRequired();

            builder
                .HasIndex(x => new { x.FirstName, x.LastName, x.Address })
                .IsUnique();
        }
    }
}
