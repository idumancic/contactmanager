﻿using ContactManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ContactManager.Persistence.Configurations
{
    public class PhoneNumberConfiguration : IEntityTypeConfiguration<PhoneNumber>
    {
        public void Configure(EntityTypeBuilder<PhoneNumber> builder)
        {
            builder.Property(x => x.Value)
                .HasMaxLength(100)
                .IsRequired();

            builder.HasOne(x => x.Contact)
                .WithMany(x => x.PhoneNumbers)
                .HasForeignKey(x => x.ContactId)
                .HasConstraintName("FK_PhoneNumbers_Contacts");

            builder.HasOne(x => x.Country)
                .WithMany(x => x.PhoneNumbers)
                .HasForeignKey(x => x.CountryId)
                .HasConstraintName("FK_PhoneNumbers_Countries");
        }
    }
}
