﻿using ContactManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ContactManager.Persistence.Configurations
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.Property(x => x.ISOAlpha2)
                .HasColumnType("char")
                .HasMaxLength(2)
                .IsRequired();

            builder.Property(x => x.ISOAlpha3)
                .HasColumnType("char")
                .HasMaxLength(3)
                .IsRequired();

            builder.Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Property(x => x.CountryCode)
                .HasMaxLength(5)
                .IsRequired();
        }
    }
}
