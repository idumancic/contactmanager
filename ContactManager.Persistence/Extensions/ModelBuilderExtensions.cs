﻿using ContactManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ContactManager.Persistence.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().HasData(
                new Country { Id = 1, Name = "Croatia", ISOAlpha2 = "HR", ISOAlpha3 = "HRV", CountryCode = "385" },
                new Country { Id = 2, Name = "United Kingdom", ISOAlpha2 = "GB", ISOAlpha3 = "GBR", CountryCode = "44" },
                new Country { Id = 3, Name = "United States", ISOAlpha2 = "US", ISOAlpha3 = "USA", CountryCode = "1" }
            );

            modelBuilder.Entity<Contact>().HasData(
                new Contact { Id = 1, FirstName = "John", LastName = "Doe", DateOfBirth = new System.DateTime(1971, 12, 1), Address = "Garrison Street 2", Created = DateTime.Now },
                new Contact { Id = 2, FirstName = "Charmaine", LastName = "Sandels", DateOfBirth = new System.DateTime(1996, 5, 17), Address = "Karstens Drive 68", Created = DateTime.Now },
                new Contact { Id = 3, FirstName = "Alexandre", LastName = "Desseine", DateOfBirth = new System.DateTime(1985, 10, 20), Address = "Arrowood Lane 45", Created = DateTime.Now }
            );

            modelBuilder.Entity<PhoneNumber>().HasData(
                new PhoneNumber { Id = 1, Value = "9537899612", CountryId = 3, ContactId = 1, Created = DateTime.Now },
                new PhoneNumber { Id = 2, Value = "9537899613", CountryId = 3, ContactId = 1, Created = DateTime.Now },
                new PhoneNumber { Id = 3, Value = "2744201872", CountryId = 2, ContactId = 2, Created = DateTime.Now },
                new PhoneNumber { Id = 4, Value = "0982342341", CountryId = 1, ContactId = 3, Created = DateTime.Now },
                new PhoneNumber { Id = 5, Value = "0982342342", CountryId = 1, ContactId = 3, Created = DateTime.Now },
                new PhoneNumber { Id = 6, Value = "2744201873", CountryId = 2, ContactId = 3, Created = DateTime.Now }
            );
        }
    }
}
