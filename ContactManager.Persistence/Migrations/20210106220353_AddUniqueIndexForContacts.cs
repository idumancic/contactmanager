﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactManager.Persistence.Migrations
{
    public partial class AddUniqueIndexForContacts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 837, DateTimeKind.Local).AddTicks(9142));

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(2715));

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(2746));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(5039));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(5056));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(5059));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(5062));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(5065));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2021, 1, 6, 23, 3, 52, 841, DateTimeKind.Local).AddTicks(5067));

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_FirstName_LastName_Address",
                table: "Contacts",
                columns: new[] { "FirstName", "LastName", "Address" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Contacts_FirstName_LastName_Address",
                table: "Contacts");

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 868, DateTimeKind.Local).AddTicks(3175));

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(3369));

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(3399));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5467));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 2,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5483));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5487));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 4,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5489));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5492));

            migrationBuilder.UpdateData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 6,
                column: "Created",
                value: new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5496));
        }
    }
}
