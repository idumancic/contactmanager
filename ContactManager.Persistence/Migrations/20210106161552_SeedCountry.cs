﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactManager.Persistence.Migrations
{
    public partial class SeedCountry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CountryCode", "ISOAlpha2", "ISOAlpha3", "Name" },
                values: new object[,]
                {
                    { 1, "385", "HR", "HRV", "Croatia" },
                    { 2, "44", "GB", "GBR", "United Kingdom" },
                    { 3, "1", "US", "USA", "United States" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
