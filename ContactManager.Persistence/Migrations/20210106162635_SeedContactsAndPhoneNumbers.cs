﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactManager.Persistence.Migrations
{
    public partial class SeedContactsAndPhoneNumbers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "Address", "Created", "DateOfBirth", "FirstName", "LastName", "Modified" },
                values: new object[,]
                {
                    { 1, "Garrison Street 2", new DateTime(2021, 1, 6, 17, 26, 34, 868, DateTimeKind.Local).AddTicks(3175), new DateTime(1971, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", "Doe", null },
                    { 2, "Karstens Drive 68", new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(3369), new DateTime(1996, 5, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Charmaine", "Sandels", null },
                    { 3, "Arrowood Lane 45", new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(3399), new DateTime(1985, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alexandre", "Desseine", null }
                });

            migrationBuilder.InsertData(
                table: "PhoneNumbers",
                columns: new[] { "Id", "ContactId", "CountryId", "Created", "Modified", "Value" },
                values: new object[,]
                {
                    { 1, 1, 3, new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5467), null, "9537899612" },
                    { 2, 1, 3, new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5483), null, "9537899613" },
                    { 3, 2, 2, new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5487), null, "2744201872" },
                    { 4, 3, 1, new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5489), null, "0982342341" },
                    { 5, 3, 1, new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5492), null, "0982342342" },
                    { 6, 3, 2, new DateTime(2021, 1, 6, 17, 26, 34, 870, DateTimeKind.Local).AddTicks(5496), null, "2744201873" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
