﻿using ContactManager.Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ContactManager.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ContactManagerContext>(options => options.UseNpgsql(configuration.GetConnectionString("ContactManagerDb")));
            services.AddScoped<IContactManagerContext>(provider => provider.GetService<ContactManagerContext>());
            return services;
        }
    }
}
